<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model
{
    protected $primarykey = 'id';

    protected $fillable = [
        'title',
        'description',
        'author',
        'publisher',
        'cover',
        'price',
        'stock'
    ];

    public function category(){
        return $this->belongsToMany('App\Category');
    }
}
