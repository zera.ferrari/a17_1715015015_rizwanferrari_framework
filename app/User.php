<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    protected $primarykey = 'id';

    protected $fillable = [
        'username',
        'email',
        'address',
        'phone',
        'status',
        'avatar',
        'password'
    ];

    protected $hidden = [
        'remember_token'
    ];

    protected $casts = [
        'email_verified_at' => 'datetime'
    ];

public function order(){
    return $this->hasMany('App\Orders', 'user_id', 'id');
    }
}
