<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book_Category extends Model
{
    protected $table = 'book_category';
    
    protected $primarykey = 'id';

    protected $fillable = [
        'book_id',
        'category_id'
    ];
}
