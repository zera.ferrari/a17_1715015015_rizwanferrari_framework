<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Orders;
use App\User;


class OrderController extends Controller
{


        public function index()
    {
   
        $StatusOrder = Orders::with('User')->paginate(5);
        return view('StatusOrder.index',['StatusOrder'=>$StatusOrder]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

        $User = User::all();
        return view('StatusOrder.create',['User'=>$User]);
    }

    public function store(Request $request)
    {
      
        $request->validate([
            'user_id'       => 'require',
            'invoice_number'=> 'require|min:8|max:32',
            'total_price'   => 'require|numeric',
            'status'        => 'require'
        ]);

        Orders::create([
            'user_id'           =>  $request->user_id,
            'invoice_number'    =>  $request->invoice_number,
            'total_price'       =>  $request->total_price,
            'status'            =>  $request->status
        ]);

        return redirect('/online-shop/StatusOrder');
    }

    public function show($id)
    {
        //
        $StatusOrder = Orders::with('User')->find($id);
        return view('StatusOrder.show',['StatusOrder'=>$StatusOrder]);
    }

    public function edit($id)
    {
        $StatusOrder = Orders::with('User')->find($id);
        $User = User::all();
        return view('StatusOrder.edit',['StatusOrder'=>$StatusOrder, 'User'=>$User]);
    }

    
    public function update(Request $request, $id)
    {
        $request->validate([
                'invoice_number'        =>  'require|min:8|max:32',
                'user_id'               =>  'require',
                'total_price'           =>  'require|numeric',
                'status'                =>  'require'
            ]);

        
        $StatusOrder=Orders::find($id);
        $StatusOrder->invoice_number    =   $request->invoice_number;
        $StatusOrder->user_id           =   $request->user_id;
        $StatusOrder->total_price       =   $request->total_price;
        $StatusOrder->status            =   $request->status;
        $StatusOrder->save();

        return redirect ('/online-shop/StatusOrder');
    }


    public function destroy($id)
    {
        
        $StatusOrder    =   Orders::find($id);
        $StatusOrder->delete();
        return redirect ('/online-shop/StatusOrder');
    }

    public function search (Request $request){
        $searching  =   $request->searching;

        $StatusOrder    =   Orders::where('status','LIKE','%'.$searching.'%')
                            ->orWhere('invoice_number','LIKE','%'.$searching.'%')->paginate();
        return view('StatusOrder.index',['StatusOrder'=>$StatusOrder]);

    }
}
