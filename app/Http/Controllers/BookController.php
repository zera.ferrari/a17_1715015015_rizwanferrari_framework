<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Books;
use App\Category;
use App\Book_Category;

class BookController extends Controller
{

    public function index()
    {

        $Book  =   Books::paginate(5);
        return view('Book.index', ['Book'=>$Book]);
    }

    public function create()
    {

        $categories     =   Category::all();
        return view('Book.create', ['categories'=>$categories]);
    }

    
    public function store(Request $request)
    {

       $request->validate([
            'title'         =>  'required|max:191',
            'author'        =>  'required|string|max:191',
            'publisher'     =>  'required|max:191',
            'price'         => 'required|numeric',
            'stock'         => 'required|numeric',
            'description'   => 'required|max:191',
            'category_name' => 'required',
            'cover'         => 'required|image',
       ]);

       if($request->hasFile('cover')){
           $gambar          =   $request->file('cover');
           $namepicture     =   '/Picture/Book/'.time().'.'.$gambar->getClientOriginalExtension();
           $directory       =   public_path('/Picture/Book');
           $gambar->move($directory, $namepicture);
       }

       Books::create([
            'title'         =>      $request->title,
            'author'        =>      $request->author,
            'publisher'     =>      $request->publisher,
            'price'         =>      $request->price,
            'stock'         =>      $request->stock,
            'description'   =>      $request->description,
            'cover'         =>      $namepicture,
       ]);

       $Book   =   Books::latest()->first();

    //    foreach($request->category_name as $genre){
    //        Book_Category::create([
    //             'book_id'       =>  $Book->id,
    //             'category_id'   =>  $genre
    //        ]);
    //    }
       return redirect ('/online-shop/Book');
    }

    
    public function show($id)
    {
        $Book  =   Books::find($id);
        return view('Book.show', ['Book'=>$Book]);   
    }

    public function edit($id)
    {
        $Book      =   Books::find($id);
        $categories =   Category::all();
        return view('Book.edit',    ['Book'=>$Book, 'categories'=>$categories]);
    }

    
    public function update(Request $request, $ID)
    {
        $request->validate([
            'title'         =>      'required|max:191',
            'author'        =>      'required|string|max:191',
            'publisher'     =>      'required|max:191',
            'price'         =>      'required|numeric',
            'stock'         =>      'required|numeric',
            'cover'         =>      'required|image',
            'description'   =>      'required|max:191',
            'category_name' =>      'required'
        ]);

        $Book = Book::find($id);
        $Book->title       =   $request->title;
        $Book->author      =   $request->author;
        $Book->publisher   =   $request->publisher;
        $Book->price       =   $request->price;
        $Book->stock       =   $request->stock;
        $Book->description =   $request->description;

        if($request->hasFile('cover')){
            $gambar         =   $request->file('cover');
            $namepicture    =   '/Picture/Book/'.time().','.$gambar->getClientOriginalExtension();
            $directory      =   public_path('/Picture/Book');
            $gambar->move($directory, $namepicture);
        }
        else{
            $namepicture    =   $Book->cover;
        }
        
        $Book->cover=$namepicture;
        $Book->save();

        $Book_categories = Book_Category::where('book_id', $id)->delete();

        // foreach($request->category_name as $genre){
        //     Book_Category::create([
        //         'book_id'       =>  $id,
        //         'category_id'   =>  $genre
        //     ]);
        // }
        return redirect('/online-shop/Book');
    }

    public function destroy($id)
    {
        $Book = Books::find($id);
        $Book->delete();
        return redirect ('/online-shop/Book');
    }

    public function search (Request $request){
        $searching  =   $request->$searching;

        $Book  =   Books::where('title','LIKE','%'.$searching.'%')->paginate();
        return view('Book.index',   ['Book'=>$Book]);
        }

}

    