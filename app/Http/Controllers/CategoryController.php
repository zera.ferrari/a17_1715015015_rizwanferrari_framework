<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;


class CategoryController extends Controller
        {
            public function index(){
                $categories     =   Category::paginate(5);
                return view('Category.index', ['categories'=>$categories]);
            }
        

   
    public function create()
    {
        //
        return view('Category.create');
    }

   
    public function store(Request $request)
    {

       $request->validate([
            'category_name' =>  'required|string',
            'description'   =>  'required|max:191'
       ]);

       Category::create([
            'category_name' =>  $request->category_name,
            'description'   =>  $request->description
       ]);

       return redirect('/online-shop/Category');

       }

       public function show($id)
    {
        //
        $categories     =   Category::find($id);
        return view('Category.show', ['categories'=>$categories]);
    }

    
    public function edit($id)
    {
        $categories     =   Category::find($id);
        return view('Category.edit', ['categories'=>$categories]);   
    }

    
     
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_name'     =>  'required|string',
            'description'       =>  'required|max:191'
        ]);   

        $categories     =   Category::find($id);
        $categories->category_name  =   $request->category_name;
        $categories->description    =   $request->description;
        $categories->save();

        return redirect('/online-shop/Category');
    }

    public function destroy($id)
    {
        $categories     =   Category::find($id);
        $categories->delete();
        return redirect('/online-shop/Category');
    }

    public function search(Request $request){
        $searching      =   $request->searching;

        $categories     =   Category::where('category_name','LIKE','%'.$searching.'%')->paginate();
        return view('Category.index', ['categories'=>$categories]);  

    }       
}