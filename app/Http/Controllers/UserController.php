<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use App\User;



class UserController extends Controller
{

        public function index(){
           $User = User::paginate(5);
           return view('User.index',['User'=>$User]);
        }

        public function create()
        {
            return view('User.create');
        }

        public function show($id){
            $User = User::find($id);
            return view('User.show',['User'=>$User]);
        }

    public function store(Request $request)
    {

        $request->validate([
            'username'    =>  'required|min:8|max:32',
            'address'     =>  'required|max:50',
            'email'       =>  'required|unique:users|email',
            'password'    =>  'required|min:6|max:32',
            'phone'       =>  'required|min:11|max:13',
            'avatar'      =>  'required|image'
        ]);

        if($request->hasFile('avatar')){
            $gambar         = $request->file('avatar');
            $namepicture    = '/Picture/User/'.time().'.'.$gambar->getClientOriginalExtension();
            $directory      = public_path('/Picture/User');
            $gambar->move($directory, $namepicture);
        }

        User::create([
            'username'      =>  $request->username,
            'address'       =>  $request->address,
            'status'        =>  "ACTIVE","INACTIVE",
            'email'         =>  $request->email,
            'password'      =>  $request->password,
            'phone'         =>  $request->phone,
            'avatar'        =>  $namepicture
        ]);

        return redirect('/online-shop/User');
    }

    public function edit($id)
    {
                $User = User::find($id);
                return view('User.edit',['User'=>$User]);
    }

    public function update(Request $request, $id)
    {
        //
        $request->validate([
            'username'          => 'required|min:8|max:32', 
            'address'        => 'required|max:50',
            'email'         => 'required|email',
            'phone'       => 'required|numeric'
        ]);

        $User = User::find($id);
        $User->username = $request->username;
        $User->address  = $request->address;
        $User->email    = $request->email;
        $User->phone    = $request->phone;

        if($request->hasFile('avatar')){
            $gambar         = $request->file('avatar');
            $namepicture    = '/Picture/User/'.time().'.'.$gambar->getClientOriginalExtension();
            $directory      = public_path('/Picture/User');
            $gambar->move($directory, $namepicture);
        }else{
            $namepicture = $User->avatar;
        }

        $User->avatar = $namepicture;
        $User->save();

        return redirect('/online-shop/User');
    }
    
    public function destroy($id)
    {
        $User = User::find($id);
        $User->delete();
        return redirect('/online-shop/User');
    }  
    
    public function search(Request $request) {
        $searching = $request->searching;

        $User = User::where('email','LIKE', '%'.$searching.'%')->paginate();
        return view('User.index', ['User'=>$User]);
    }
}
