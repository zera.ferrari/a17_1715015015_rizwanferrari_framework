<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Orders extends Model
{
    protected $primarykey = 'id';

    protected $fillable = [
        'user_id',
        'total_price',
        'invoice_number',
        'status'
    ];

    public function user(){
        return $this->belongsTo('App\User', 'user_id', 'id');
    } 
}
