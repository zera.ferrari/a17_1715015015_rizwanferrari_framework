<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/online-shop', function (){
    return view('Template/Home');
});


Route::group(['prefix'=>'/online-shop'], function(){

    Route::resource('/User','UserController');
    Route::resource('/Book','BookController');
    Route::resource('/Category','CategoryController');
    Route::resource('/StatusOrder','OrderController');

    Route::get('/User/search/email','UserController@search')->name('User.search');
    Route::get('/StatusOrder/search/invoice_number','OrderController@search')->name('StatusOrder.search');
    Route::get('/Book/search/title','BookController@search')->name('Book.search');
    Route::get('/Category/search/category_name','CategoryController@search')->name('Category.search');
});