@extends('Template.Home')
    @section('title')
        Change Book
    @endsection

@section('content')
    <div class="container">
        <h3>Form Change Book</h3>
        <hr>

            <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
                <div class="card-header bg-primary text-white">
                    <h5>{{ $Book['title'] }}</h5>
                </div>

                <div class="card-body">
                    <div class="container text-primary">

                        <form action="{{ route('Book.update',$Book['id']) }}" method="POST" class="form-group"
                        enctype="multipart/form-data">

                        @csrf
                        @method('PUT')
                            <div class="row">
                            <div class="col-md-3">

                                <label for="title" class="text-primary">BookName</label>
                            </div>

                            <div class="col-md-8">
                                <input type="text" name="title" id="title" value="{{ $Book->title }}">
                                {{($errors->has('title')) ? $errors->first('title') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="author" class="text-primary">Author</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="author" id="author" value="{{ $Book->author }}">
                                {{($errors->has('author')) ? $errors->first('author') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="publisher" class="text-primary">Publisher</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="publisher" id="publisher" class="form-control" value="{{ $Book->publisher }}">
                                {{($errors->has('publisher')) ? $errors->first('publisher') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="price" class="text-primary">Price</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="price" id="price" class="form-control" value="{{ $Book->price }}">
                                {{($errors->has('price')) ? $errors->first('price') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="stock" class="text-primary">Stock</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" name="stock" id="stock" class="form-control" value="{{ $Book->stock }}">
                                {{($errors->has('stock')) ? $errors->first('stock') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="cover" class="text-primary">Cover</label>
                            </div>
                            <div class="col-md-8">
                                <img src="{{asset($Book->cover) }}" class="img-thumbnail"
                                alt="gambar" height="150px" width="150px">
                                <div class="custom-file">
                                     <label for="CustomFile" class="custom-file-label">File Cover</label>
                                         <input type="file" name="cover" id="CustomFile" class="custom-file-input">
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="description" class="text-primary">Description</label>
                            </div>
                            <div class="col-md-8">
                                <input type="textarea" name="description" id="description" class="form-control" value="{{ $Book->description }}">
                                {{($errors->has('description')) ? $errors->first('description') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="category_name" class="text-primary">Genre</label>
                            </div>
                            <div class="col-md-8">
                                <select name="category_name" id="category_name" class="form-control">
                                    @foreach($categories as $category)
                                    <option value="{{ $category->id}}">{{ $category->category_name}}</option>
                                    @endforeach
                                </select>
                                {{($errors->has('category_name')) ? $errors->first('category_name') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-md-5">
                                <button type="submit" class="btn btn-outline-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
         </div>
    </div>
@endsection
