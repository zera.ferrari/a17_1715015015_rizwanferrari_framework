@extends('Template.Home')
@section('title')
    Create Book
@endsection

@section('content')
    <div class="container">
        <h3>Create Book</h3>
        <hr>
            <div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
                <div class="card-header bg-primary text-white">
                    <h5>Create a New Book</h5>
                </div>

                <div class="card-body">
                    <div class="contrainer text-primary">
                        <form action="{{ route('Book.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="row">
                                    <div class="col-md-3">

                                        <label for="title">Book Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="title" class="form-control" id="title">
                                        {{($errors->has('title')) ? $errors->first('title') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">

                                        <label for="author">Author</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="author" id="author" class="form-control">
                                        {{($errors->has('author')) ? $errors->first('author') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">

                                        <label for="publisher">Publisher</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="publisher" id="publisher" class="form-control">
                                        {{($errors->has('publisher')) ? $errors->first('publisher') : ""}}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="price" class="text-primary">Price</label>
                                    </div>
        
                                    <div class="col-md-8">
                                        <input type="text" name="price" id="price" class="form-control">
                                        {{($errors->has('price')) ? $errors->first('price') : ""}}
                                    </div>
                                </div>
                                <br>
                                
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="stock" class="text-primary">Stock</label>
                                    </div>
        
                                    <div class="col-md-8">
                                        <input type="text" name="stock" id="stock" class="form-control">
                                        {{($errors->has('stock')) ? $errors->first('stock') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="description" class="text-primary">description</label>
                                    </div>
        
                                    <div class="col-md-8">
                                        <input type="text" name="description" id="description" class="form-control">
                                        {{($errors->has('description')) ? $errors->first('description') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="category_name" class="text-primary">Category Book</label>
                                    </div>
        
                                    <div class="col-md-8">
                                        <select name="category_name" id="category_name" class="form-control">
                                            @foreach ($categories as $category)
                                                <option value="{{ $category->id}}">{{ $category->category_name }}</option>
                                            @endforeach
                                        </select>
                                        {{($errors->has('category_name')) ? $errors->first('category_name') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="input-group mb-3">
                                        <div class="col-md-3 text-primary">
                                              Cover
                                        </div>
                                    <div class="col-md-8">
                                        <div class="custom-file">
                                            <label for="cover" class="custom-file-label">Select Cover</label>
                                            <input class="custom-file-input" type="file" name="cover" id="cover">
                                        </div>
                                        {{ ($errors->has('cover')) ? $errors->first('cover') : "" }}
                                    </div>
                                </div>
                            </div>
                            <br>
                                <div class="row">
                                    <div class="col-md-3 offset-md-5 offset-sm-4">
                                        <button type="submit" class="btn btn-outline-primary">Create</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
@endsection
