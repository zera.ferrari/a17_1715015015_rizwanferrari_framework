@extends('Template.Home')
    @section('title')
        Detail Book
    @endsection

@section('content')
    <h1>Detail Book</h1>
    <hr>
    <br>
            <div class="card bg-white border-info" style="max-witdh: 70%; margin: auto; min-height: 400px;">
                <div class="row" style="padding;25px">
                    <div class="col-md-2 offset-md-5 offset-sm-4">

                        <img src="{{asset($Book['cover']) }}" style="height:150px; width:150px;" class="rounded-circle" alt="cover">
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 text-center">

                        <h3>{{ $Book['title'] }}</h3>
                    </div>
                </div>
                <hr>
                <br>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Author
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{ $Book['author']}}
                    </div>
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Publisher
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{$Book['publisher']}}
                    </div>
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Deskripsi
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{$Book['description']}}
                    </div>
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Price
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{$Book['price']}}
                    </div>
                    <br>
                </div>

                {{-- <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Genre
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <ul>
                        @foreach ($Book->category as $category)
                            <li>{{ $category->category_name }}</li>
                        @endforeach
                        </ul>
                    </div>		
                </div> --}}

            </div>
