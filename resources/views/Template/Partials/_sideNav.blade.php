<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="{{route('User.index')}}">
                    <span data-feather="users"></span>
                    User
                </a>
            </li>
        </ul>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="{{route('StatusOrder.index')}}">
                    <span data-feather="list"></span>
                    StatusOrder
                </a>
            </li>
        </ul>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="{{route('Book.index')}}">
                    <span data-feather="book"></span>
                    Book
                </a>
            </li>
        </ul>

        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="{{route('Category.index')}}">
                    <span data-feather="shopping-cart"></span>
                    Category
                </a>
            </li>
        </ul>
    </div>
</nav>
