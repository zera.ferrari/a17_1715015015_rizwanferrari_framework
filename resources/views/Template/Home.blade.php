<html lang="en">
@include('Template.Partials._head')
    <body>
        @include('Template.Partials._topNav')
            <div class="container-fluid">
                <div class="row">
                    @include('Template.Partials._sideNav')
                        <main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
                            @yield('content')
                        </main>
                </div>
            </div>
            <footer class="footer">
                <div class="container text-center text-light">
                    <span>&copy; Rizwan Ferrari</span>
                </div>
            </footer>
            @include('Template.Partials._script')
    </body>
</html>
