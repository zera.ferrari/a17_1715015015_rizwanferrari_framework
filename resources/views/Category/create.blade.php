@extends('Template.Home')
@section('title')
    Create CategoryBook
@endsection

@section('content')
    <div class="container">
        <h3>Create Category</h3>
        <hr>
            <div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
                <div class="card-header bg-primary text-white">
                    <h5>Create a New CategoryBook</h5>
                </div>

                <div class="card-body">
                    <div class="contrainer text-primary">
                        <form action="{{ route('Category.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="category_name">Category Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="category_name" class="form-control" id="category_name">
                                        {{($errors->has('category_name')) ? $errors->first('category_name') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="description" class="text-primary">Description</label>
                                    </div>
        
                                    <div class="col-md-8">
                                        <input type="text" name="description" id="description">
                                        {{($errors->has('description')) ? $errors->first('description') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3 offset-md-5 offset-sm-4">
                                        <button type="submit" class="btn btn-outline-primary">Create</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
@endsection
