@extends('Template.Home')
    @section('title')
        Category Detail
    @endsection

@section('content')
    <h1>Category Detail</h1>
        <hr>
            <br>
            <div class="card bg-white border-info" style="max-witdh: 70%; margin: auto; min-height: 400px;">


                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>{{ $categories['category_name'] }}</h3>
                    </div>
                </div>
                <hr>
                <br>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        ID
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{ $categories['id']}}
                    </div>
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Description
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{ $categories['description']}}
                    </div>
                    <br>
                </div>
            <br>
        </div>
@endsection
