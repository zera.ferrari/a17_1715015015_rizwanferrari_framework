@extends('Template.Home')
    @section('title')
        Edit Category
    @endsection

@section('content')
    <div class="container">
        <h3>Form Edit Category Book</h3>
        <hr>

            <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
                <div class="card-header bg-primary text-white">
                    <h5>{{ $categories['category_name'] }}</h5>
                </div>

                <div class="card-body">
                    <div class="container text-primary">
                        <form action="{{ route('Category.update',$categories['id']) }}" method="POST" class="form-group"
                        enctype="multipart/form-data">

                        @csrf
                        @method('PUT')
                            <div class="row">
                            <div class="col-md-3">
                                <label for="category_name" class="text-primary">Category Name</label>
                            </div>

                            <div class="col-md-8">
                                <input type="text" name="category_name" id="category_name" value="{{ $categories['category_name'] }}">
                                {{($errors->has('category_name')) ? $errors->first('category_name') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="description" class="text-primary">Description</label>
                            </div>

                            <div class="col-md-8">
                                <input type="text" name="description" id="description" value="{{ $categories['description'] }}">
                                {{($errors->has('description')) ? $errors->first('description') : ""}}s
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-md-5">
                                <button type="submit" class="btn btn-outline-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
@endsection
