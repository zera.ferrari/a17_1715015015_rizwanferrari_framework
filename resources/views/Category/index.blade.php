@extends('Template.Home')
            @section('title')
                List Category
            @endsection
        @section('css')
        <style>
            body{
                padding-top: 30px;
            }

            th, td{
                padding: 10px;
                text-align: center;
            }

            td a{
                margin: 3px;
                align-content: center;
                color: white;
            }

            td a:hover{
                text-decoration: none;
            }

            td button{
                margin-top: 5px;
                cursor: pointer;
            }
        </style>
        @endsection
        @section('content')
            <div class="container">
                <h3> List Category </h3><hr>
                    <div class="row">
                        <div class="col-md-2">
                            <a class="btn btn-outline-primary" href=" {{ route('Category.create')}} ">
                                <span data-feather="plus-circle"></span>
                                New<span class="sr-only">(current)</span>
                            </a>
                        </div>
                        <div class="col-md-8">
                            <form action="{{ route('Category.search')}}" class="form-inline" method="GET">
                                <div class="form-group mx-sm-3 mb-2">
                                    <input class="form-control" name="searching" placeholder="Search Category .." value="{{old ('searching')}}">
                                </div>
                                <button class="btn btn-primary mb-2" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table=striped">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col">ID</th>
                                    <th scope="col">Genre</th>
                                    <th scope="col">Description</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $kategori)
                                    <tr>
                                        <td>{{ $kategori->id }}</td>
                                        <td>{{ $kategori->category_name }}</td>
                                        <td>{{ $kategori->description}}</td>
                                        {{-- <td>{{ $kategori->Book->title}}</td> --}}
                                        
                                        <td>
                                        <a class="btn-sm btn-primary" href="{{ route('Category.show', $kategori['id']) }}">
                                            <span data-feather="eye"></span>
                                            Detail <span class="sr-only">(current)</span>
                                        </a>


                                        <a class="btn-sm btn-success d-inline" href="{{route('Category.edit',$kategori['id']) }}">
                                            <span data-feather="edit-2"></span>
                                            Edit <span class="sr-only">(current)</span>
                                        </a>
                                        <form class="d-inline" onsubmit="return confirm('Delete This User Permanently ?')"

                                        action="{{route ('Category.destroy', $kategori['id']) }}" method="POST">
                                            @csrf
                                                @method('DELETE')
                                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                                        <span data-feather="trash"></span>
                                                        Delete
                                                        <span class="sr-only">(current)</span>
                                                    </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$categories->links()}}
                </div>
            </div>
        @endsection


