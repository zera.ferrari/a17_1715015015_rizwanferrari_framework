@extends('Template.Home')
            @section('title')
                List Order
            @endsection
        @section('css')
        <style>
            body{
                padding-top: 30px;
            }

            th, td{
                padding: 10px;
                text-align: center;
            }

            td a{
                margin: 3px;
                align-content: center;
                color: white;
            }

            td a:hover{
                text-decoration: none;
            }

            td button{
                margin-top: 5px;
                cursor: pointer;
            }
        </style>
        @endsection
        @section('content')
            <div class="container">
                <h3> List Order</h3><hr>
                    <div class="row">
                        <div class="col-md-2">
                            <a class="btn btn-outline-primary" href=" {{ route('StatusOrder.create')}} ">
                                <span data-feather="plus-circle"></span>
                                New<span class="sr-only">(current)</span>
                            </a>
                        </div>

                        <div class="col-md-8">
                            <form action="{{ route('StatusOrder.search') }}" class="form-inline" method="GET">
                                <div class="form-group mx-sm-3 mb-2">
                                    <input class="form-control" name="searching" placeholder="Search InvoiceNumber ..." value="{{ old('searching') }}" >
                                </div>
                                <button class="btn btn-primary mb-2" type="submit">Search</button>
                            </form>
                        </div>
                    </div>
                    <br>

                    <div class="table-responsive">
                        <table class="table table=striped">
                            <thead>
                                <tr class="table-primary">
                                    <th scope="col">ID</th>
                                    <th scope="col">Username</th>
                                    <th scope="col">InvoiceNumber</th>
                                    <th scope="col">Total Price</th>
                                    <th scope="col">Status</th>
                                    <th scope="col">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($StatusOrder as $order)
                                    <tr>
                                        <td>{{ $order->id }}</td>
                                        <td>{{ $order->User->username }}</td>
                                        <td>{{ $order->invoice_number }}</td>
                                        <td>{{ $order->total_price }}</td>
                                        <td>
                                            @if ($order['status'] == 'PROCESS')
                                            <div class="p-1 mb-2 bg-warning text-dark">{{ $order->status }}</div>

                                            @elseif($order['status'] == 'SUBMIT')
                                            <div class="p-1 mb-2 bg-warning text-white">{{ $order->status }}</div>

                                            @elseif($order['status'] == 'FINISH')
                                            <div class="p-1 mb-2 bg-warning text-purple">{{ $order->status }}</div>

                                            @elseif($order['status'] == 'CANCEL')
                                            <div class="p-1 mb-2 bg-warning text-grey">{{ $order->status }}</div>
                                            @endif
                                        </td>
                                        <td>
                                        <a class="btn-sm btn-primary" href="{{ route('StatusOrder.show', ['id'=>$order['id']]) }}">
                                            <span data-feather="eye"></span>
                                            Detail <span class="sr-only">(current)</span>
                                        </a>

                                        <a class="btn-sm btn-success d-inline" href="{{route('StatusOrder.edit',['id'=>$order['id']]) }}">
                                            <span data-feather="edit-2"></span>
                                            Edit <span class="sr-only">(current)</span>
                                        </a>
                                        <form class="d-inline" onsubmit="return confirm('Delete This User Permanently ?')"
                                        action="{{route ('StatusOrder.destroy', ['id'=>$order['id']]) }}" method="POST">
                                            @csrf
                                                @method('DELETE')
                                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                                        <span data-feather="trash"></span>
                                                        Delete
                                                        <span class="sr-only">(current)</span>
                                                    </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    {{ $StatusOrder->links()}}
                </div>
            </div>
        @endsection


