@extends('Template.Home')
@section('title')
    Create Order
@endsection

@section('content')
    <div class="container">
        <h3>Create Order</h3>
        <hr>
            <div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
                <div class="card-header bg-primary text-white">
                    <h5>Create a New Order</h5>
                </div>

                <div class="card-body">
                    <div class="contrainer text-primary">
                        <form action="{{ route('StatusOrder.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="username">Username</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="user_id" id="status" class="form-control">
                                            @foreach($User as $users)
                                            <option value="{{ $users->id }}">{{$users->username}}</option>
                                            @endforeach
                                        </select>
                                        {{($errors->has('user_id')) ? $errors->first('user_id') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="invoice_number">InvoiceNumber</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="invoice_number" id="invoice_number" class="form-control">
                                        {{($errors->has('invoice_number')) ? $errors->first('invoice_number') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="total_price">Total Price</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="total_price" id="total_price" class="form-control">
                                        {{($errors->has('total_price')) ? $errors->first('total_price') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="status">status</label>
                                    </div>
                                    <div class="col-md-8">
                                        <select name="status" id="status" class="form-control">
                                            <option value="SUBMIT">SUBMIT</option>
                                            <option value="PROCESS">PROCESS</option>
                                            <option value="FINISH">FINISH</option>
                                            <option value="CANCEL">CANCEL</option>
                                        </select>
                                        {{($errors->has('status')) ? $errors->first('status') : ""}}
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-3 offset-md-5 offset-sm-4">
                                        <button type="submit" class="btn btn-outline-primary">Create</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
    </div>
@endsection
