@extends('Template.Home')
    @section('title')
        Detail Order
    @endsection

@section('content')
    <h1>Detail Order </h1>
        <hr>
            <br>
            <div class="card bg-white border-info" style="max-witdh: 70%; margin: auto; min-height: 400px;">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h3>Username : {{ $StatusOrder->User->username}} </h3>
                        <h3>ID : {{$StatusOrder->id }}</h3>
                    </div>
                </div>
                <hr>
                <br>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Invoice Number
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{ $StatusOrder->invoice_number }}
                    </div>
                    <br>
                </div>

                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Total Price
                    </div>
                    <div class="col-md-4 col-sm-4">
                        {{ $StatusOrder->total_price }}
                    </div>
                    <br>
                </div>


                <div class="row">
                    <div class="col-md-2 offset-md-2 col-sm-3 offset-sm-2">
                        Status
                    </div>
                    <div class="col-md-4 col-sm-4">
                        @if ($StatusOrder['status'] == 'PROCESS')
                        <div class="p-1 mb-2 bg-warning text-dark">{{ $StatusOrder->status }}</div>
                        @elseif ($StatusOrder['status'] == 'SUBMIT')
                        <div class="p-1 mb-2 bg-warning text-dark">{{ $StatusOrder->status }}</div>
                        @elseif ($StatusOrder['status'] == 'FINISH')
                        <div class="p-1 mb-2 bg-warning text-dark">{{ $StatusOrder->status }}</div>
                        @elseif ($StatusOrder['status'] == 'CANCEL')
                        <div class="p-1 mb-2 bg-warning text-dark">{{ $StatusOrder->status }}</div>
                        @endif
                    </div>
                    <br>
                </div>
            </div>
@endsection
