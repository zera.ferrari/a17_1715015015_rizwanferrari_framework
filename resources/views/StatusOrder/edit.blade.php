@extends('Template.Home')
    @section('title')
        Edit Order
    @endsection

@section('content')
    <div class="container">
        <h3>Form Edit Order</h3>
        <hr>

            <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
                <div class="card-header bg-primary text-white">

                    <h5>ID: {{ $StatusOrder->invoice_number }}</h5>
                    <h5>Username : {{ $StatusOrder->User->username }}</h5>
                </div>

                <div class="card-body">
                    <div class="container text-primary">
                        <form action="{{ route('StatusOrder.update', $StatusOrder['id']) }}" method="POST" class="form-group"
                        enctype="multipart/form-data">

                        @csrf
                        @method('PUT')
                            <div class="row">
                            <div class="col-md-3">
                                <label for="invoice_number" class="text-primary">Invoice Number</label>
                            </div>

                            <div class="col-md-8">
                                <input type="text" name="invoice_number" id="invoice_number" value="{{ $StatusOrder['invoice_number'] }}">
                                {{ ($errors->has('invoice_number')) ? $errors->first('invoice_number') : "" }}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="username">Username</label>
                            </div>

                            <div class="col-md-8">
                                <select class="form-control" name="user_id" id="status">
                                    @foreach($User as $users)
                                    <option value=" {{ $users->id }}"> {{ $users->username }} </option>
                                    @endforeach
                                </select>
                                {{($errors->has('user_id')) ? $errors->first('user_id') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="total_price" class="text-primary">Total Price</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="total_price" id="total_price" value="{{ $StatusOrder['total_price'] }}">
                                {{ ($errors->has('total_price')) ? $errors->first('total_price') : ""}}
                            </div>
                        </div>
                        <br>

                        
                        <div class="row">
                            <div class="col-md-3">
                                <label for="status" class="text-primary">Status</label>
                            </div>
                            <div class="col-md-8">
                                <select class="form-control" name="status" id="status">
                                    <option value="SUBMIT">SUBMIT</option>
                                    <option value="PROCESS">PROCESS</option>
                                    <option value="CANCEL">CANCEL</option>
                                    <option value="FINISH">FINISH</option>
                                </select>
                                {{ ($errors->has('status')) ? $errors-first('status') : "" }}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-md-5">
                                <button type="submit" class="btn btn-outline-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
@endsection
