@extends('Template.Home')
            @section('title')
                List User
            @endsection
        @section('css')
        <style>
            body{
                padding-top: 30px;
            }

            th, td{
                padding: 10px;
                text-align: center;
            }

            td a{
                margin: 3px;
                align-content: center;
                color: white;
            }

            td a:hover{
                text-decoration: none;
            }

            td button{
                margin-top: 5px;
                cursor: pointer;
            }
        </style>
        @endsection
        @section('content')
            <div class="container">
                <h3> List User</h3><hr>
                    <div class="row">
                        <div class="col-md-2">
                            <a class="btn btn-outline-primary" href=" {{ route('User.create')}} ">
                                <span data-feather="plus-circle"></span>
                                New<span class="sr-only">(current)</span>
                            </a>
                        </div>
                        <div class="col-md-8">
                            <form action="{{ route('User.search') }}" class="form-inline" method="GET">
                                <div class="form-group mx-sm-3 mb-2">
                                    <input class="form-control" name="searching" placeholder="Search Email ..." value="{{ old('searching') }}" >
                                </div>
                                <button class="btn btn-primary mb-2" type="submit">Search</button>
                            </form>
                        </div>

                        <br>
                        <div class="table-responsive">
                        <table class="table table-striped">
                        <thead>
                            <tr class="table-primary">
                                <th scope="col">ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Picture</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>

                                @foreach ($User as $user)
                                    <tr>
                                        <td>{{ $user->id }}</td>
                                        <td>{{ $user->username }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->phone }}</td>

                                        <td>
                                            <img src="{{ asset($user->avatar) }}" alt="avatar" width="80px">
                                        </td>
                                        <td>
                                        <a class="btn-sm btn-primary" href="{{ route('User.show', ['id'=>$user->id]) }}">
                                            <span data-feather="eye"></span>Detail <span class="sr-only">(curret)</span>
                                        <a class="btn-sm btn-success d-inline" href="{{route('User.edit',['id'=>$user->id]) }}">
                                            <span data-feather="edit-2"></span>
                                            Edit <span class="sr-only">(current)</span>
                                        </a>
                                        <form class="d-inline" onsubmit="return confirm('Delete This User Permanently ?')"
                                        action="{{route ('User.destroy', $user->id) }}" method="POST">
                                            @csrf
                                                @method('DELETE')
                                                    <button type="submit" class="btn-sm btn-danger" value="Delete">
                                                        <span data-feather="trash"></span>
                                                        Delete
                                                        <span class="sr-only">(current)</span>
                                                    </button>
                                        </form>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{$User->links()}}
                </div>
            </div>
        @endsection


