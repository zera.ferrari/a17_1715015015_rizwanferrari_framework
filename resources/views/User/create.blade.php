@extends('Template.Home')
@section('title')
    Create User
@endsection

@section('content')
    <div class="container">
        <h3>Create User</h3>
        <hr>
            <div class="card border-primary" style="max-width: 70%; margin: auto; margin-top: 40px;">
                <div class="card-header bg-primary text-white">
                    <h5>Create a New User</h5>
                </div>

                <div class="card-body">
                    <div class="contrainer text-primary">
                        <form action="{{ route('User.store') }}" class="form-group" method="POST" enctype="multipart/form-data">
                            @csrf
                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="username">Name</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="username" class="form-control" id="username" value="{{ old('username') }}">
                                        {{($errors->has('username')) ? $errors->first('username') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="email">Email</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="text" name="email" id="email" class="form-control" value="{{ old('email')}}" cols="30" rows="10">
                                        {{($errors->has('email')) ? $errors->first('email') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="password">Password</label>
                                    </div>

                                    <div class="col-md-8">
                                        <input type="password" name="password" id="password" class="form-control" value="{{ old('password')}} " cols="30" rows="10">
                                        {{($errors->has('password')) ? $errors->first('password') : ""}}
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="address">Address</label>
                                    </div>
                                    <div class="col-md-8">
                                        <textarea name="address" id="address" cols="20" rows="5" class="form-control" value="{{old('address')}}"></textarea>
                                        {{($errors->has('address')) ? $errors->first('address') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="phone">Phone</label>
                                    </div>
                                    <div class="col-md-8">
                                        <input type="number" name="phone" id="phone" class="form-control" value="{{ old('phone')}}">
                                        {{($errors->has('phone')) ? $errors->first('phone') : ""}}
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3">
                                        <label for="status">Status</label>
                                    </div>
                                    <div class="col-md-8">
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="status" id="ACTIVE" class="custom-control-input" value="ACTIVE">
                                                <label for="ACTIVE" class="custom-control-label">ACTIVE</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" name="status" id="INACTIVE" class="custom-control-input" value="INACTIVE">
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="input-group mb-3">
                                        <div class="col-md-3 text-primary">
                                            Avatar
                                        </div>
                                        <div class="col-md-8">
                                            <div class="custom-file">
                                                <label for="avatar" class="custom-the-label">Avatar</label>
                                                    <input type="file" name="avatar" id="avatar" class="custom-file-input">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br>

                                <div class="row">
                                    <div class="col-md-3 offset-md-5 offset-sm-4">
                                        <button type="submit" class="btn btn-outline-primary">Create</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
      </div>
@endsection
