@extends('Template.Home')
    @section('title')
        Edit User
    @endsection

@section('content')
    <div class="container">
        <h3>Form Edit User</h3>
        <hr>

            <div class="card border-primary" style="max-width: 70%; margin:auto; margin-top:40px;">
                <div class="card-header bg-primary text-white">

                    <h5>{{ $User->username }}</h5>
                </div>

                <div class="card-body">
                    <div class="container text-primary">
                        <form action="{{ route('User.update',['id'=>$User->id]) }}" method="POST" class="form-group"
                        enctype="multipart/form-data">

                        @csrf
                        @method('PUT')
                            <div class="row">
                            <div class="col-md-3">
                                <label for="username" class="text-primary">Name</label>
                            </div>

                            <div class="col-md-8">
                                <input class="form-control" type="text" name="username" id="username" value="{{ $User->username }}">
                                {{ ($errors->has('username')) ? $errors->first('username'): ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="address" class="text-primary">Address</label>
                            </div>

                            <div class="col-md-8">
                                <input class="text-primary" name="address" id="address" type="text" value="{{ $User->address }} ">
                                {{ ($errors->has('address')) ? $errors->first('address') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="email" class="text-primary">Email</label>
                            </div>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="email" id="email" value="{{ $User->email }}">
                                {{ ($errors->has('email'))? $errors->first('email') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="phone" class="text-primary">Phone</label>
                            </div>
                            <div class="col-md-8">
                                <input type="number" name="phone" id="phone" class="form-control" value="{{ $User->phone }}">
                                {{ ($errors->has('phone')) ? $errors->first('phone') : ""}}
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3">
                                <label for="status" class="text-primary">Status</label>
                            </div>
                            <div class="col-md-8">
                                <select name="status" id="status" class="form-control">
                                    <option value="SUBMIT">SUBMIT</option>
                                    <option value="PROCESS">PROCESS</option>
                                    <option value="FINISH">FINISH</option>
                                    <option value="CANCEL">CANCEL</option>
                                </select>
                                    {{ ($errors->has('status')) ? $errors->first('status') : ""}}
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="input-group mb-3">
                                <div class="col-md-3 text-primary">
                                    Avatar
                                </div>
                                <div class="col-md-8">
                                    <img src="{{asset($User->avatar) }}" class="img-thumbnail"
                                    alt="gambar" height="150px" width="150px">
                                    <div class="custom-file">
                                         <label for="CustomFile" class="custom-file-label">Avatar</label>
                                             <input type="file" name="avatar" id="CustomFile" class="custom-file-input">
                                    </div>
                                    {{ ($errors->has('avatar')) ? $errors->first('avatar') : ""}}
                                </div>
                            </div>
                        </div>
                        <br>

                        <div class="row">
                            <div class="col-md-3 offset-md-5 offset-md-5">
                                <button type="submit" class="btn btn-outline-primary">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
    </div>
@endsection
