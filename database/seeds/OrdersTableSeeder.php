<?php

use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders')->insert([

            [
                'id'                => '1',
                'user_id'           =>  '1',
                'total_price'       =>  '100000',
                'invoice_number'    =>  '1A3E8B23K12',
                'status'            =>  'SUBMIT'
            ],

            [
                'id'                => '2',
                'user_id'           =>  '1',
                'total_price'       =>  '105000',
                'invoice_number'    =>  'J7392301KD1',
                'status'            =>  'PROCESS'
            ],

            [
                'id'                => '3',
                'user_id'           =>  '2',
                'total_price'       =>  '110000',
                'invoice_number'    =>  '7ADK34J9F0S',
                'status'            =>  'FINISH'
            ],

            [
                'id'                => '4',
                'user_id'           =>  '2',
                'total_price'       =>  '115000',
                'invoice_number'    =>  '9O1G4YDK3MD',
                'status'            =>  'CANCEL'
            ],

            [
                'id'                => '5',
                'user_id'           =>  '3',
                'total_price'       =>  '120000',
                'invoice_number'    =>  '8U31G82KM48',
                'status'            =>  'SUBMIT'
            ],

            [
                'id'                => '6',
                'user_id'           =>  '3',
                'total_price'       =>  '125000',
                'invoice_number'    =>  'Y83F7GD23MS',
                'status'            =>  'PROCESS'
            ],

            [
                'id'                => '7',
                'user_id'           =>  '4',
                'total_price'       =>  '130000',
                'invoice_number'    =>  'UY83HF381MD',
                'status'            =>  'FINISH'
            ],

            [
                'id'                => '8',
                'user_id'           =>  '4',
                'total_price'       =>  '135000',
                'invoice_number'    =>  '73KDMW2J31H',
                'status'            =>  'PROCESS'
            ],

            [
                'id'                => '9',
                'user_id'           =>  '5',
                'total_price'       =>  '120000',
                'invoice_number'    =>  'MB48FH2K34M',
                'status'            =>  'PROCESS'
            ],

            [
                'id'                => '10',
                'user_id'           =>  '5',
                'total_price'       =>  '100000',
                'invoice_number'    =>  'NB2F68Z93MS',
                'status'            =>  'CANCEL'
            ],

            [
                'id'                => '11',
                'id'                => '11',
                'user_id'           =>  '6',
                'total_price'       =>  '150000',
                'invoice_number'    =>  'GH102SM3JDN',
                'status'            =>  'FINISH'
            ],

            [
                'id'                => '12',
                'user_id'           =>  '6',
                'total_price'       =>  '145000',
                'invoice_number'    =>  'BI19MDLS73JS',
                'status'            =>  'FINISH'
            ],

            [
                'id'                => '13',
                'user_id'           =>  '7',
                'total_price'       =>  '130000',
                'invoice_number'    =>  'YW18NB3LS83',
                'status'            =>  'SUBMIT'
            ],

            [
                'id'                => '14',
                'user_id'           =>  '7',
                'total_price'       =>  '165000',
                'invoice_number'    =>  'UY28MSN39S01',
                'status'            =>  'PROCESS'
            ],

            [
                'id'                => '15',
                'user_id'           =>  '7',
                'total_price'       =>  '100000',
                'invoice_number'    =>  'JYUQ4K29D31',
                'status'            =>  'FINISH'
            ]

        ]);
    }
}
