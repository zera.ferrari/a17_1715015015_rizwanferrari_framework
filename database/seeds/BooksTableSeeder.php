<?php

use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
                'title'         =>  'War',
                'description'   =>  'The World Be End',
                'author'        =>  'Michael Nowl',
                'publisher'     =>  'BigStar',
                'cover'         =>  '/Picture/Book/1.jpg',
                'price'         =>  '100000',
                'stock'         =>  '5'
            ],

            [
                'title'         =>  'Medicine For Life',
                'description'   =>  'The Medicine is sick',
                'author'        =>  'Reza Apriandi',
                'publisher'     =>  'BigStar',
                'cover'         =>  '/Picture/Book/2.jpg',
                'price'         =>  '120000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'Food Nice',
                'description'   =>  'Food enak gak tau ah beli truck',
                'author'        =>  'Richardo Milos',
                'publisher'     =>  'Revolution',
                'cover'         =>  '/Picture/Book/3.jpg',
                'price'         =>  '100000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'Horus Life',
                'description'   =>  'The Immortal Life',
                'author'        =>  'Seto Kaiba',
                'publisher'     =>  'Kaiba Corporation',
                'cover'         =>  '/Picture/Book/4.jpg',
                'price'         =>  '125000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'Laugh of Laugh',
                'description'   =>  'Ketawa ni hehe',
                'author'        =>  'Ucup S.Komedian',
                'publisher'     =>  '1cak',
                'cover'         =>  '/Picture/Book/5.jpg',
                'price'         =>  '120000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'Style John Cena',
                'description'   =>  'The invisible Man',
                'author'        =>  'Andi Magelang S.Komedian',
                'publisher'     =>  '1forfun',
                'cover'         =>  '/Picture/Book/6.jpg',
                'price'         =>  '100000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'Art Of Magician',
                'description'   =>  'The Magician Art in the world',
                'author'        =>  'Deddy Corbuzer',
                'publisher'     =>  'HitamWhite',
                'cover'         =>  '/Picture/Book/7.jpg',
                'price'         =>  '150000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'The Great Country',
                'description'   =>  'Indonesian Great Country',
                'author'        =>  'Putri Dewi.S.S',
                'publisher'     =>  'TransMedia',
                'cover'         =>  '/Picture/Book/8.jpg',
                'price'         =>  '160000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  '1001 Island',
                'description'   =>  'Beautiful Indonesian Country',
                'author'        =>  'Maulana Ferrari Barkhawi',
                'publisher'     =>  'BeautifulIndonesia',
                'cover'         =>  '/Picture/Book/9.jpg',
                'price'         =>  '200000',
                'stock'         =>  '100'
            ],

            [
                'title'         =>  'How To Use Invoker',
                'description'   =>  'Fast Hand With This Hero',
                'author'        =>  'Rizwan Ferrari',
                'publisher'     =>  'Steam',
                'cover'         =>  '/Picture/Book/10.jpg',
                'price'         =>  '150000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'History Of Dota',
                'description'   =>  'The History Of Dota',
                'author'        =>  'Gaben',
                'publisher'     =>  'Steam',
                'cover'         =>  '/Picture/Book/11.jpg',
                'price'         =>  '100000',
                'stock'         =>  '2'
            ],

            [
                'title'         =>  'Technology in Future',
                'description'   =>  'Technology in Future Robot',
                'author'        =>  'Bill Gates',
                'publisher'     =>  'Windows',
                'cover'         =>  '/Picture/Book/12.jpg',
                'price'         =>  '50000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'Relationship',
                'description'   =>  'Women stalking Men',
                'author'        =>  'Vina Mower',
                'publisher'     =>  'Relation',
                'cover'         =>  '/Picture/Book/13.jpg',
                'price'         =>  '125000',
                'stock'         =>  '5'
            ],

            [
                'title'         =>  'Love in Paris',
                'description'   =>  'Someone first time see love',
                'author'        =>  'Hawa Jihan',
                'publisher'     =>  'Option',
                'cover'         =>  '/Picture/Book/14.jpg',
                'price'         =>  '120000',
                'stock'         =>  '10'
            ],

            [
                'title'         =>  'What next we are',
                'description'   =>  'What World in future',
                'author'        =>  'Awkarin',
                'publisher'     =>  'LampLight',
                'cover'         =>  '/Picture/Book/15.jpg',
                'price'         =>  '150000',
                'stock'         =>  '50'
            ]

        ]);

        
    }
}
