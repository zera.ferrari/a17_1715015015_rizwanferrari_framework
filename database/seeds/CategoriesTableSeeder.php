<?php

use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            [
                'category_name'     =>  'Lifestyle',
                'description'       =>  'Lifestyle is good'
            ],

            [
                'category_name'     =>  'Comedy',
                'description'       =>  'Comedy is good'
            ],

            [
                'category_name'     =>  'Medicine',
                'description'       =>  'Medicine is good'
            ],

            [
                'category_name'     =>  'Food',
                'description'       =>  'Food is good'
            ],

            [
                'category_name'     =>  'War',
                'description'       =>  'War be end'
            ],

            [
                'category_name'     =>  'Art',
                'description'       =>  'Art is Beautiful'
            ],

            [
                'category_name'     =>  'Religion',
                'description'       =>  'Religion is life'
            ],

            [
                'category_name'     =>  'Country',
                'description'       =>  'Country style'
            ],

            [
                'category_name'     =>  'game',
                'description'       =>  'game is art for fun'
            ],

            [
                'category_name'     =>  'romance',
                'description'       =>  'romance not good'
            ],

            [
                'category_name'     =>  'future',
                'description'       =>  'what next future'
            ],

            [
                'category_name'     =>  'history',
                'description'       =>  'history is old life'
            ],

            [
                'category_name'     =>  'justice',
                'description'       =>  'justice for everyone'
            ],

            [
                'category_name'     =>  'economy',
                'description'       =>  'economy people'
            ],

            [
                'category_name'     =>  'Technology',
                'description'       =>  'technology for future'
            ]
        ]);
    }
}
