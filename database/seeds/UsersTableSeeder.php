<?php

use Illuminate\Database\Seeder;
// use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([

            [
                'id'      => '1',
                'username'=> 'Rizwan',
                'email'   => 'rizwan@gmail.com',
                'address' => 'jalan.bumiindah',
                'phone'   => '08223455667788',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/az.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '2',
                'username'=> 'Ferrari',
                'email'   => 'ferrari@gmail.com',
                'address' => 'jalan.earthbeauti',
                'phone'   => '081122334455',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/2.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '3',
                'username'=> 'Kadal',
                'email'   => 'kadal@gmail.com',
                'address' => 'jalan.hutan',
                'phone'   => '089988776655',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/3.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '4',
                'username'=> 'Shit',
                'email'   => 'shit@gmail.com',
                'address' => 'jalan.lier',
                'phone'   => '089823456789',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/4.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '5',
                'username'=> 'Boolean',
                'email'   => 'truefalse@gmail.com',
                'address' => 'jalan.kebenaran',
                'phone'   => '081345629876',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/5.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '6',
                'username'=> 'Wrong',
                'email'   => 'wrong@gmail.com',
                'address' => 'jalan.kesalahan',
                'phone'   => '081250063932',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/6.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '7',
                'username'=> 'True',
                'email'   => 'true@gmail.com',
                'address' => 'jalan.antara',
                'phone'   => '08376483923',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/2.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '8',
                'username'=> 'God',
                'email'   => 'god@gmail.com',
                'address' => 'jalan.heaven',
                'phone'   => '081122339511',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/3.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '9',
                'username'=> 'Horse',
                'email'   => 'horse@gmail.com',
                'address' => 'jalan.kuda',
                'phone'   => '089756734953',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/5.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '10',
                'username'=> 'Trojan',
                'email'   => 'trojan@gmail.com',
                'address' => 'jalan.trojan',
                'phone'   => '085613945856',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/az.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '11',
                'username'=> 'Worm',
                'email'   => 'worm@gmail.com',
                'address' => 'jalan.kecilgesit',
                'phone'   => '087634566673',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/6.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '12',
                'username'=> 'Malware',
                'email'   => 'malware@gmail.com',
                'address' => 'jalan.wormmalware',
                'phone'   => '087642349284',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/3.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '13',
                'username'=> 'Ramnit',
                'email'   => 'ramnit@gmail.com',
                'address' => 'jalan.kerusakan',
                'phone'   => '084729340482',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/5.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '14',
                'username'=> 'Iga',
                'email'   => 'iga@gmail.com',
                'address' => 'jalan.enak',
                'phone'   => '082167493045',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/6.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '15',
                'username'=> 'Tama',
                'email'   => 'tama@gmail.com',
                'address' => 'jalan.keindahan',
                'phone'   => '087834214038',
                'status'  => 'INACTIVE',
                'avatar'  => '/Picture/User/az.jpg',
                'password'=>Hash::make("112233")
            ],

            [
                'id'      => '16',
                'username'=> 'Barkawi',
                'email'   => 'barkawi@gmail.com',
                'address' => 'jalan.pangeran',
                'phone'   => '084729230573',
                'status'  => 'ACTIVE',
                'avatar'  => '/Picture/User/2.jpg',
                'password'=>Hash::make("112233")
            ],
        ]);
    }
}
